const path = require('path');

/**
 * Webpack configuration for Finly
 */

module.exports = env => {
  /**
   * commonConfig
   * This is the configuration object which is common for all.
   */
  let commonConfig = {
    mode: env && env.production === true ? 'production' : 'development',
    //Node polyfills
    node: {
      process: true,
    },
    watch: env && env.production === true ? false : true,

    //set minification flag
    optimization: {
      minimize: env && env.production === true ? true : false,
    },

    //webpack bundle analyzer
    //can be removed if large bundles slows down compilation
    // plugins: env && env.production === true ? [] : [new BundleAnalyzerPlugin()],

    //set up babel transpiler
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          resolve: {
            extensions: ['.js', '.jsx'],
          },
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-object-rest-spread'],
          },
          exclude: /node_modules/,
        }
      ],
    },
    //set console logs in color
    stats: {
      colors: true,
    },
    //include source-map in builds
    // devtool: env && env.production === true ? [] : ['cheap-source-map'],
    entry: path.resolve(__dirname, './index.js'),
  };
  
  return commonConfig;
};

