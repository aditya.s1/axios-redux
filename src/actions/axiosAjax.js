/**
 * Axios Ajax Library - Wrapper
 */

/**
 * Import Axios library
 */
import axios from 'axios';

/**
 * Import axios configuration object
 */
import axiosConfig from './axiosConfig';
import environmentHandler from '../helpers/environmentHandler';

/**
 * Base64 encode payload
 */
let payload = environmentHandler({
  identifier: 'clientData',
  config: {},
  key: undefined 
});


/**
 * @class AxiosAjax
 * Axios Ajax class
 */
function AxiosAjax(options) {
  /**
   * initialize the data and attributes
   */
  /**
   * Options for axios
   */
  this.options = options || axiosConfig || {};

  /**
   * Custom headers
   * Authorization token can be added here
   */
  this.options.headers = {
    Authorization: 'Bearer ' + environmentHandler({
      identifier: 'storage',
      config: {
        type: 'get',
      },
      key: 'accessToken' 
    }),
    currentDevice: environmentHandler({
      identifier: 'storage',
      config: {
        type: 'get',
      },
      key: 'currentDevice' 
    }),
    'X-Finly-RP': payload != undefined ? payload : '',
  };

  /**
   * Initialize the axios instance
   */
  this.http = axios.create(this.options);

  /**
   * @name makeRequest
   * @inner
   * Make a request with the options and parameters provided.
   * @param {String} url - The url string
   * @param {String} method - The HTTP method
   * @param {Object} queryParameters - The query parameters
   * @param {Object} body - The request body
   */
  this.makeRequest = (url, method, queryParameters, body, current_app) => {
    this.url = url
      ? url
      : (() => {
          throw new Error('URL required');
        })();
    this.queryParameters = queryParameters || {};
    this.body = body || {};
    this.method = method || 'get';
    this.current_app = current_app || 'vp';

    /**
     * Make the request
    */

    let URL = environmentHandler({
      identifier: 'host',
      config: {
        value: this.url
      }
    });

    let request = this.http({
      method: this.method,
      url: URL,
      params: this.queryParameters,
      data: this.body,
      headers: {
        App: this.current_app,
      },
    });
    return request;
  };

  /**
   * @name fetchUrl
   * @inner
   * Fetch a url with the options and parameters provided.
   * @param {String} url - The url string
   * @param {String} method - The HTTP method
   * @param {Object} queryParameters - The query parameters
   * @param {Object} body - The request body
   */
  this.fetchUrl = (url, method, queryParameters, body) => {
    return this.makeRequest(
      url,
      method.toLowerCase(),
      (queryParameters = queryParameters ? queryParameters : ''),
      (body = body ? body : ''),
    )
      .then(function(res) {
        if (res.status === 403 || res.status === 401) {
          /**
           * Redirect to login page
           */
          environmentHandler({
            identifier: 'redirect',
            key: 'unauthorized'
          })
        } else {
          return res.data;
        }
      })
      .catch(function(err) {
        return Promise.reject(err);
      });
  };
}

export default AxiosAjax;
