/**
 * Import reducers here
 */

/* DYNAMIC INDEX - Reducers */

import documentAttachmentsReducer from './reducers/documentAttachmentsReducer';

import { AttachmentRequests } from './actions/actions';

/**
 * Combine all the reducers and export
 */
export const reducersList = {
  'documentAttachmentsReducer' : documentAttachmentsReducer
}

export const actionsList = AttachmentRequests;


