import {
  ADD_ATTACHMENT_TO_DOCUMENT_SUCCESS_RESPONSE,
  ADD_ATTACHMENT_TO_DOCUMENT_REQUEST,
  ADD_ATTACHMENT_TO_DOCUMENT_ERROR_RESPONSE,
} from './types.js';

import prepareAJAXCall from './prepareAJAXCall.js';

export const documentAttachments = ajax_config => {
  return dispatch => {
    dispatch(request());
    return prepareAJAXCall(ajax_config)
      .then(result => {
        dispatch(successResponse(result));
        result.data.fileInfo = ajax_config.fileInfo;
        return ajax_config.callback(result);
      })
      .catch(error => {
        dispatch(errorResponse(error));
        return ajax_config.callback(error);
      });
  };
};

export const request = () => ({
  type: ADD_ATTACHMENT_TO_DOCUMENT_REQUEST,
});

export const successResponse = response => {
  return {
    type: ADD_ATTACHMENT_TO_DOCUMENT_SUCCESS_RESPONSE,
    payload: response,
  };
};

export const errorResponse = error => ({
  type: ADD_ATTACHMENT_TO_DOCUMENT_ERROR_RESPONSE,
  payload: { error },
});
