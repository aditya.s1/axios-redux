import { documentAttachments } from './documentAttachments';

const ActionMappings = {
  'documentAttachments' : documentAttachments
}

export const AttachmentRequests = ActionMappings;