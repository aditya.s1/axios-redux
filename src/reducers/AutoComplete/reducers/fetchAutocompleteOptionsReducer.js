/**
 * Import actions here
 */
import {
  AUTOCOMPLETE_OPTIONS_REQUEST,
  AUTOCOMPLETE_OPTIONS_SUCCESS_RESPONSE,
  AUTOCOMPLETE_OPTIONS_ERROR_RESPONSE,
  AUTOCOMPLETE_OPTIONS_RESET,
} from './../actions/types';

/**
 * Set up initial state (not mandatory)
 */
const initialState = {
  result: {},
  loading: false,
  error: null,
};

/**
 * @function fetchAutocompleteOptionsReducer
 * A reducer to reduce menu data subscribed - actions
 * @param {*} state - The default state
 * @param {*} action - The dispatched actions
 * @returns {Object} - The respective state w.r.t the action dispatched
 */
let fetchAutocompleteOptionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTOCOMPLETE_OPTIONS_REQUEST:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        loading: true,
        error: null,
      };

    case AUTOCOMPLETE_OPTIONS_SUCCESS_RESPONSE:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        loading: false,
        result: action.payload.data
          ? action.payload.data.result ||
            action.payload.data.results ||
            action.payload.data.data ||
            action.payload.data
          : action.payload,
      };

    case AUTOCOMPLETE_OPTIONS_ERROR_RESPONSE:
      // The request failed, but it did stop, so set loading to "false".
      // Save the error, and we can display it somewhere
      // Since it failed, we don't have items to display anymore, so set it empty.
      // This is up to you and your app though: maybe you want to keep the items
      // around! Do whatever seems right.
      return {
        ...state,
        loading: false,
        error: action.payload,
        result: {},
      };

    case AUTOCOMPLETE_OPTIONS_RESET:
      return {
        ...state,
        loading: false,
        error: {},
        result: {},
      };

    default:
      // ALWAYS have a default case in a reducer
      return state;
  }
};

export default fetchAutocompleteOptionsReducer;
