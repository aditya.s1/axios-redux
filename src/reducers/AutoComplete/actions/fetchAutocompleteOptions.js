import prepareAJAXCall from './prepareAJAXCall';

import {
  AUTOCOMPLETE_OPTIONS_REQUEST,
  AUTOCOMPLETE_OPTIONS_SUCCESS_RESPONSE,
  AUTOCOMPLETE_OPTIONS_ERROR_RESPONSE,
  AUTOCOMPLETE_OPTIONS_RESET
} from './types';


export const fetchAutocompleteOptions = ajax_config => {
  let callback_identifier = ajax_config.url_config ? ajax_config.url_config.type : undefined;
  return dispatch => {
    if(ajax_config.reset){
      return dispatch(reset());
    }
    dispatch(request());
    return prepareAJAXCall(ajax_config)
      .then(result => {
        dispatch(successResponse(result));
        result = result.data.result || result.data.results || result.data.data || result.data;
        return ajax_config.callback(result, callback_identifier);
      })
      .catch(error => {
        dispatch(errorResponse(error));
        return ajax_config.callback(error, callback_identifier);
      });
  };
};

export const request = () => ({
  type: AUTOCOMPLETE_OPTIONS_REQUEST,
});

export const successResponse = response => {
  return {
    type: AUTOCOMPLETE_OPTIONS_SUCCESS_RESPONSE,
    payload: response,
  };
};

export const errorResponse = error => ({
  type: AUTOCOMPLETE_OPTIONS_ERROR_RESPONSE,
  payload: { error },
});

export const reset = response => {
  return {
    type: AUTOCOMPLETE_OPTIONS_RESET,
    payload: response,
  };
};
