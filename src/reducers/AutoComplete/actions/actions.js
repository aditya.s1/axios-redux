import { fetchAutocompleteOptions } from './fetchAutocompleteOptions';

const ActionMappings = {
  'fetchAutocompleteOptions' : fetchAutocompleteOptions
}

export const AutocompleteRequests = ActionMappings;