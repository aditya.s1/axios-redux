/**
 * Import reducers here
 */

/* DYNAMIC INDEX - Reducers */

import fetchAutocompleteOptionsReducer from './reducers/fetchAutocompleteOptionsReducer';

import { AutocompleteRequests } from './actions/actions';

/**
 * Combine all the reducers and export
 */
export const reducersList = {
  'fetchAutocompleteOptionsReducer' : fetchAutocompleteOptionsReducer
}

export const actionsList = AutocompleteRequests;


