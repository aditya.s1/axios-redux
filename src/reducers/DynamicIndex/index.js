/**
 * Import reducers here
 */

/* DYNAMIC INDEX - Reducers */

import fetchDynamicFiltersConfigReducer from './reducers/fetchDynamicFiltersConfigReducer';
import fetchDynamicIndexCountReducer from './reducers/fetchDynamicIndexCountReducer';
import fetchDynamicIndexActionsReducer from './reducers/fetchDynamicIndexActionsReducer';
import fetchDynamicIndexListReducer from './reducers/fetchDynamicIndexListReducer';
import fetchDynamicIndexConfigReducer from './reducers/fetchDynamicIndexConfigReducer';
import fetchDynamicStatusFiltersReducer from './reducers/fetchDynamicStatusFiltersReducer';
import fetchDynamicViewsConfigReducer from './reducers/fetchDynamicViewsConfigReducer';
import documentViewsCrudReducer from './reducers/documentViewsCrudReducer';

import { DynamicIndexRequests } from './actions/actions';

/**
 * Combine all the reducers and export
 */
export const reducersList = {
  'fetchDynamicFiltersConfigReducer' : fetchDynamicFiltersConfigReducer,
  'fetchDynamicIndexConfigReducer' : fetchDynamicIndexConfigReducer,
  'fetchDynamicIndexListReducer' : fetchDynamicIndexListReducer,
  'fetchDynamicIndexActionsReducer' : fetchDynamicIndexActionsReducer,
  'fetchDynamicIndexCountReducer' : fetchDynamicIndexCountReducer,
  'fetchDynamicStatusFiltersReducer' : fetchDynamicStatusFiltersReducer,
  'fetchDynamicViewsConfigReducer' : fetchDynamicViewsConfigReducer,
  'documentViewsCrudReducer' : documentViewsCrudReducer
}

export const actionsList = DynamicIndexRequests;


