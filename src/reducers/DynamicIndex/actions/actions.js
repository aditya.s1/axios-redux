import { fetchDynamicFiltersConfig } from './fetchDynamicFiltersConfig';
import { fetchDynamicIndexCount } from './fetchDynamicIndexCount';
import { fetchDynamicIndexActions } from './fetchDynamicIndexActions';
import { fetchDynamicIndexList } from './fetchDynamicIndexList';
import { fetchDynamicIndexConfig } from './fetchDynamicIndexConfig';
import { fetchDynamicStatusFilters } from './fetchDynamicStatusFilters';
import { fetchDynamicViewsConfig } from './fetchDynamicViewsConfig';
import { documentViewsCRUD } from './documentViewsCrud';

const ActionMappings = {
  'fetchDynamicFiltersConfig' : fetchDynamicFiltersConfig,
  'fetchDynamicIndexCount' : fetchDynamicIndexCount,
  'fetchDynamicIndexActions' : fetchDynamicIndexActions,
  'fetchDynamicIndexList' : fetchDynamicIndexList,
  'fetchDynamicIndexConfig' : fetchDynamicIndexConfig,
  'fetchDynamicStatusFilters' : fetchDynamicStatusFilters,
  'fetchDynamicViewsConfig' : fetchDynamicViewsConfig,
  'documentViewsCrud' : documentViewsCRUD,
}

export const DynamicIndexRequests = ActionMappings;