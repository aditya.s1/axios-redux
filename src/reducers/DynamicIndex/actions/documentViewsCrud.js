import prepareAJAXCall from './prepareAJAXCall';

import {
  DOCUMENT_VIEWS_CRUD_REQUEST,
  DOCUMENT_VIEWS_CRUD_SUCCESS_RESPONSE,
  DOCUMENT_VIEWS_CRUD_ERROR_RESPONSE
} from './types';


export const documentViewsCRUD = ajax_config => {
  let callback_identifier = ajax_config.url_config.type;
  return dispatch => {
    dispatch(request());
    return prepareAJAXCall(ajax_config)
      .then(result => {
        dispatch(successResponse(result));
        return ajax_config.callback(result, callback_identifier);
      })
      .catch(error => {
        dispatch(errorResponse(error));
        return ajax_config.callback(error, callback_identifier);
      });
  };
};

export const request = () => ({
  type: DOCUMENT_VIEWS_CRUD_REQUEST,
});

export const successResponse = response => {
  return {
    type: DOCUMENT_VIEWS_CRUD_SUCCESS_RESPONSE,
    payload: response,
  };
};

export const errorResponse = error => ({
  type: DOCUMENT_VIEWS_CRUD_ERROR_RESPONSE,
  payload: { error },
});
