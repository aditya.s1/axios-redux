import AxiosAjax from './../../../actions/axiosAjax';
import URL_SCHEMA from '../../../../lib/urlSchema.json';

let prepareAJAXCall = ajax_config => {
  let raw_url, method, ajax;
  
  if(ajax_config.url_config.method == 'post'){
    ajax = new AxiosAjax({ headers: {'Content-Type': 'multipart/form-data' }});
  }else{
    ajax = new AxiosAjax();
  }

  raw_url = URL_SCHEMA.root_url + ajax_config.url_config.url; 

  let params = convertJSONTOParams(ajax_config.params);
  if(params.length){
    raw_url += '?' + params
  }

  method = ajax_config.url_config.method;

  return ajax.makeRequest(
    raw_url,
    method,
    null,
    ajax_config.formData
  );

};

let convertJSONTOParams = json => {
  return Object.keys(json)
    .map(function(k) {
      if (k != 'model_id') {
        return encodeURIComponent(k) + '=' + encodeURIComponent(json[k]);
      }
    })
    .join('&');
};

export default prepareAJAXCall;