import prepareAJAXCall from './prepareAJAXCall';

import {
  FETCH_DYNAMIC_STATUS_FILTERS_REQUEST,
  FETCH_DYNAMIC_STATUS_FILTERS_SUCCESS_RESPONSE,
  FETCH_DYNAMIC_STATUS_FILTERS_ERROR_RESPONSE,
  FETCH_DYNAMIC_STATUS_FILTERS_RESET
} from './types';


export const fetchDynamicStatusFilters = ajax_config => {
  let callback_identifier = ajax_config.url_config ? ajax_config.url_config.type : undefined;
  return dispatch => {
    if (ajax_config.reset){
      return dispatch(resetStore());
    } else {
      dispatch(request());
    }
    return prepareAJAXCall(ajax_config)
      .then(result => {
        dispatch(successResponse(result));
        return ajax_config.callback(result, callback_identifier);
      })
      .catch(error => {
        dispatch(errorResponse(error));
        return ajax_config.callback(error, callback_identifier);
      });
  };
};

export const request = () => ({
  type: FETCH_DYNAMIC_STATUS_FILTERS_REQUEST,
});

export const successResponse = response => {
  return {
    type: FETCH_DYNAMIC_STATUS_FILTERS_SUCCESS_RESPONSE,
    payload: response,
  };
};

export const errorResponse = error => ({
  type: FETCH_DYNAMIC_STATUS_FILTERS_ERROR_RESPONSE,
  payload: { error },
});

export const resetStore = () => ({
  type: FETCH_DYNAMIC_STATUS_FILTERS_RESET,
});
