import prepareAJAXCall from './prepareAJAXCall';

import {
  FETCH_DYNAMIC_INDEX_ACTIONS_REQUEST,
  FETCH_DYNAMIC_INDEX_ACTIONS_SUCCESS_RESPONSE,
  FETCH_DYNAMIC_INDEX_ACTIONS_ERROR_RESPONSE
} from './types';


export const fetchDynamicIndexActions = ajax_config => {
  let callback_identifier = ajax_config.url_config.type;
  return dispatch => {
    dispatch(request());
    return prepareAJAXCall(ajax_config)
      .then(result => {
        dispatch(successResponse(result));
        return ajax_config.callback(result, callback_identifier);
      })
      .catch(error => {
        dispatch(errorResponse(error));
        return ajax_config.callback(error, callback_identifier);
      });
  };
};

export const request = () => ({
  type: FETCH_DYNAMIC_INDEX_ACTIONS_REQUEST,
});

export const successResponse = response => {
  return {
    type: FETCH_DYNAMIC_INDEX_ACTIONS_SUCCESS_RESPONSE,
    payload: response,
  };
};

export const errorResponse = error => ({
  type: FETCH_DYNAMIC_INDEX_ACTIONS_ERROR_RESPONSE,
  payload: { error },
});
