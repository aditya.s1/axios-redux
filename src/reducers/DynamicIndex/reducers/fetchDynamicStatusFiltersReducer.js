/**
 * Import actions here
 */
import {
  FETCH_DYNAMIC_STATUS_FILTERS_REQUEST,
  FETCH_DYNAMIC_STATUS_FILTERS_SUCCESS_RESPONSE,
  FETCH_DYNAMIC_STATUS_FILTERS_ERROR_RESPONSE,
  FETCH_DYNAMIC_STATUS_FILTERS_RESET,
} from './../actions/types';

/**
 * Set up initial state (not mandatory)
 */
const initialState = {
  result: {},
  loading: false,
  error: null,
};

/**
 * @function fetchDynamicStatusFiltersReducer
 * A reducer to reduce menu data subscribed - actions
 * @param {*} state - The default state
 * @param {*} action - The dispatched actions
 * @returns {Object} - The respective state w.r.t the action dispatched
 */
let fetchDynamicStatusFiltersReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DYNAMIC_STATUS_FILTERS_REQUEST:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        loading: true,
        error: null,
      };

    case FETCH_DYNAMIC_STATUS_FILTERS_SUCCESS_RESPONSE:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        loading: false,
        result: action.payload,
      };

    case FETCH_DYNAMIC_STATUS_FILTERS_ERROR_RESPONSE:
      // The request failed, but it did stop, so set loading to "false".
      // Save the error, and we can display it somewhere
      // Since it failed, we don't have items to display anymore, so set it empty.
      // This is up to you and your app though: maybe you want to keep the items
      // around! Do whatever seems right.
      return {
        ...state,
        loading: false,
        error: action.payload,
        result: {},
      };
    
    case FETCH_DYNAMIC_STATUS_FILTERS_RESET:
      return {
        result: {},
        loading: false,
        error: null,
      }

    default:
      // ALWAYS have a default case in a reducer
      return state;
  }
};

export default fetchDynamicStatusFiltersReducer;
