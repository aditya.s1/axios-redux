export default function _isReactNative(){
  if (typeof document !== 'undefined') {
    return false; 
  }
  else if (typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
    return true;
  }
  return false;
}