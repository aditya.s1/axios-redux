export default function redirect(identifier, _isApp){
  if(identifier == 'unauthorized'){
    return _isApp ? undefined : window.location.href = '/login';
  }
}