import { nativeDataSets } from "./dataSets";

export default function storage(identifier, _isApp, config){
  let _TYPE =  config.type == 'get' ? 'getItem' : 'setItem';
  return _isApp ? nativeDataSets[identifier] : localStorage[_TYPE](identifier, config.value);
}