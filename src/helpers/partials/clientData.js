import environmentHandler from "../environmentHandler";
import _isReactNative from './_isReactNative';

export default function clientData(){
  let payload;

  if(_isReactNative){
    payload = 'eyIxMyI6eyIxIjoiQXJpYWwsIEJhdWhhdXMgOTMsIENvdXJpZXIgTmV3LCBLYWNzdE9uZSwgTG9oaXQgR3VqYXJhdGksIExvbWEsIE1TIEdvdGhpYywgTVMgUEdvdGhpYywgTVMgUE1pbmNobywgTWVubG8sIFBNaW5nTGlVLCBSYWNoYW5hLCBTYXdhc2RlZSwgU2ltU3VuLCBUaW1lcyBOZXcgUm9tYW4sIFVidW50dSwgVW1wdXNoLCAiLCIyIjoiIiwiMyI6dHJ1ZSwiNCI6IiwgUG9ydGFibGUgRG9jdW1lbnQgRm9ybWF0LCBOYXRpdmUgQ2xpZW50IEV4ZWN1dGFibGUsIFBvcnRhYmxlIE5hdGl2ZSBDbGllbnQgRXhlY3V0YWJsZSIsIjUiOiJlbi1JTiIsIjYiOiIiLCI3IjoxNjU2ODExNzQsIjgiOiJXZWJLaXQiLCI5Ijp0cnVlLCIxMCI6MTkyMCwiMTEiOjE1OTEwOTMzMjQwMzIsIjEyIjoiIiwiMTMiOjI0LCIxNCI6dHJ1ZSwiMTUiOmZhbHNlLCIxNiI6Ik1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzc4LjAuMzkwNC4xMDggU2FmYXJpLzUzNy4zNiIsIjE3IjoiIiwiMTgiOiJMaW51eCIsIjE5IjoiSW5kaWEgU3RhbmRhcmQgVGltZSIsIjIwIjoiIiwiMjEiOiI3OC4wLjM5MDQuMTA4IiwiMjIiOnRydWUsIjIzIjoiNTM3LjM2IiwiMjQiOjEwODAsIjI1IjoieDg2XzY0IiwiMjYiOiIiLCIyNyI6IkNocm9tZSIsIjI4IjoiQ2hyb21lIFBERiBQbHVnaW4sIENocm9tZSBQREYgVmlld2VyLCBOYXRpdmUgQ2xpZW50IiwiMjkiOmZhbHNlLCIzMCI6ZmFsc2UsIjMxIjpmYWxzZX19';
  } else {
    payload = WebData();
  }

  return payload;
}

function WebData(){
  let ClientJS = require('clientjs');
  /**
   * ClientJS
   * ClientJS is a simple library for finding device information and generating digital fingerprints.
   * @see {@link https://clientjs.org/| ClientJS.org}
   * Here ClientJS is accessed from global browser window object, assuming the library is already available from rails
   */
  client = new ClientJS();
  let clientData = {
    '13': {
      '11': Date.now(),
      '16': client.getUserAgent() || '',
      '19': client.getTimeZone() || '',
      '7': client.getFingerprint() || '',
      '5': client.getLanguage() || '',
      '20': client.getSystemLanguage() || '',
      '27': client.getBrowser() || '',
      '21': client.getBrowserVersion() || '',
      '8': client.getEngine() || '',
      '23': client.getEngineVersion() || '',
      '18': client.getOS() || '',
      '25': client.getOSVersion() || '',
      '10': window.screen.width,
      '24': window.screen.height,
      '2': client.getDeviceXDPI() || '',
      '26': client.getDeviceYDPI() || '',
      '13': client.getColorDepth() || '',
      '15': client.isJava(),
      '17': client.getJavaVersion() || '',
      '30': client.isFlash(),
      '12': client.getFlashVersion() || '',
      '29': client.isSilverlight(),
      '6': client.getSilverlightVersion() || '',
      '9': client.isMimeTypes(),
      '4': client.getMimeTypes() || '',
      '1': client.getFonts() || '',
      '3': client.isLocalStorage(),
      '14': client.isSessionStorage(),
      '22': client.isCookie(),
      '28': client.getPlugins() || '',
      '31': client.isMobile(),
    },
  };

  /**
   * Base64 encode payload
   */
  payload = btoa(JSON.stringify(clientData));
  environmentHandler({
    identifier: 'storage',
    config: {
      type: 'set',
      value: payload
    },
    key: 'xFinlyRp' 
  });
  return payload;
}