import storage from "./partials/storage";
import redirect from "./partials/redirect";
import host from "./partials/host";
import _isReactNative from "./partials/_isReactNative";
import clientData from "./partials/clientData";

const mappings = {
  'storage': storage,
  'redirect': redirect,
  'host': host,
  'clientData': clientData
}

export default function environmentHandler({ identifier, key, config }){
  let _isApp = _isReactNative();
  return mappings[identifier](key, _isApp, config || undefined);
}