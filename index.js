import {
  actionsList as DIActions,
  reducersList as DIReducers
} from "./src/reducers/DynamicIndex";

import {
  actionsList as LookupActions,
  reducersList as LookupReducers
} from "./src/reducers/AutoComplete";

import {
  actionsList as AttachmentActions,
  reducersList as AttachmentReducers
} from "./src/reducers/Attachments";


const CONFIG = {
  reducers: {
    'DynamicIndex': DIReducers,
    'AutoComplete': LookupReducers,
    'Attachments': AttachmentReducers
  },
  actions: {
    'DynamicIndex': DIActions,
    'AutoComplete': LookupActions,
    'Attachments': AttachmentActions
  }
}

export default CONFIG;